package com.investimentos.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.investimentos.investimentos.enums.RiscoInvestimento;
import com.investimentos.investimentos.models.Investimento;
import com.investimentos.investimentos.models.Simulacao;
import com.investimentos.investimentos.models.Simular;
import com.investimentos.investimentos.services.InvestimentoService;
import com.investimentos.investimentos.services.SimulacaoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.management.InvalidAttributeValueException;
import java.util.Optional;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTests {
    @MockBean
    SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Simular simular;

    @Test
    public void testarSimulacaoDeInvestimento() throws Exception {
        Investimento investimentoEntrada = new Investimento();
        investimentoEntrada.setId(1);
        investimentoEntrada.setNome("Mariana vai");
        investimentoEntrada.setDescricao("VFOooooi");
        investimentoEntrada.setRiscoInvestimento(RiscoInvestimento.ALTO);
        investimentoEntrada.setPercentual(0.50);
        Simular simular = new Simular(investimentoEntrada,10,100);

        Simulacao simulacao = new Simulacao();
        simulacao.setResultadoSimulacao(1050.00);
        Mockito.when(simulacaoService.simulacarInvestimento(Mockito.any(Simular.class))).thenReturn(simulacao);

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultadoSimulacao", CoreMatchers.equalTo(1050.00)));
    }

}
