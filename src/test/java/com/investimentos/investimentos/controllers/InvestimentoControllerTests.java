package com.investimentos.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.investimentos.investimentos.enums.RiscoInvestimento;
import com.investimentos.investimentos.models.Investimento;
import com.investimentos.investimentos.services.InvestimentoService;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(InvestimentoController.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InvestimentoControllerTests {
    @MockBean
    InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Investimento investimento;

    @BeforeEach
    public void iniciar() {
        investimento = new Investimento("BCFF11", "Fundo de Investimento Imobiliário", RiscoInvestimento.BAIXO, 0.47);
    }

    @Test
    public void testarBuscarInvestimento() throws Exception {
        investimento.setId(1);
        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Mockito.when(investimentoService.buscarPorId(1)).thenReturn(investimentoOptional);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1").contentType(MediaType.APPLICATION_JSON)
                    .content(json)).andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("BCFF11")));
        }

    @Test
    public void testarBuscarInvestimentoErro() throws Exception {
        investimento.setId(1);
        Optional<Investimento> investimentoOptional = Optional.of(investimento);

        Mockito.when(investimentoService.buscarPorId(1)).thenReturn(investimentoOptional);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/2").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarCriarInvestimento() throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);
        String json = mapper.writeValueAsString(investimento);

            mockMvc.perform(MockMvcRequestBuilders.post("/investimentos").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("BCFF11")));
    }

    @Test
    public void testarAtualizarInvestimento() throws Exception{
        investimento.setId(1);
        Optional<Investimento> retorno = Optional.of(investimento);
        Investimento investimentoComAlteracao = new Investimento();
        investimentoComAlteracao = investimento;
        investimentoComAlteracao.setDescricao("Fundo de Investimento Imobiliário.");
        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo("Fundo de Investimento Imobiliário.")));
    }

    @Test
    public void testarDeletarLead() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo("Fundo de Investimento Imobiliário")));

        Mockito.verify(investimentoService, Mockito.times(1)).deletarInvestimento(investimento);
    }

    @Test
    public void testarBuscarTodos() throws Exception {
        Iterable<Investimento> listInvestimentos = Arrays.asList();
        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(listInvestimentos);
        String json = mapper.writeValueAsString(listInvestimentos);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)));
    }

    @Test
    public void testarBuscarTodosError() throws Exception {
        Iterable<Investimento> listInvestimentos = Arrays.asList(investimento);
        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenThrow(new RuntimeException());
        String json = mapper.writeValueAsString(listInvestimentos);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarDeletarLeadError() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(investimento));
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/0")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.verify(investimentoService, Mockito.times(1)).deletarInvestimento(investimento);
    }
}
