package com.investimentos.investimentos.services;

import com.investimentos.investimentos.models.Usuario;
import com.investimentos.investimentos.repositories.UsuarioRepository;
import com.investimentos.investimentos.security.DetalhesUsuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@SpringBootTest
public class UsuarioServiceTests {

    @MockBean
    UsuarioRepository usuarioRepository;

    @Autowired
    UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void inicializar(){
        usuario = new Usuario();
        usuario.setSenha("senha");
        usuario.setEmail("marianaafonso@gmail.com");
        usuario.setNome("Mariana Afonso");
    }

    @Test
    public void testarSalvarUsuario(){
        Mockito.when(usuarioRepository.findByEmail("Teste@teste.com")).thenReturn(null);
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);
        Assertions.assertEquals(usuario, usuarioObjeto);
    }

    @Test
    public void testarSalvarUsuarioError(){
        Mockito.when(usuarioRepository.findByEmail("marianaafonso@gmail.com")).thenReturn(usuario);
        Assertions.assertThrows(RuntimeException.class, () -> usuarioService.salvarUsuario(usuario));
    }

    @Test
    public void testarLoadUserByUserName(){
        DetalhesUsuario usuarioDetalhes = new DetalhesUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        Mockito.when(usuarioRepository.findByEmail("marianaafonso@gmail.com")).thenReturn(usuario);
        UserDetails retorno = usuarioService.loadUserByUsername("marianaafonso@gmail.com");
        Assertions.assertTrue(retorno.getPassword() == usuario.getSenha());
    }

    @Test
    public void testarLoadUserByUserNameError(){
        DetalhesUsuario usuarioDetalhes = new DetalhesUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        Mockito.when(usuarioRepository.findByEmail("marianaafonso@gmail.com")).thenReturn(null);
        Assertions.assertThrows(UsernameNotFoundException.class, () -> usuarioService.loadUserByUsername("marianaafonso@gmail.com"));
    }


}
