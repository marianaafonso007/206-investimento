package com.investimentos.investimentos.services;

import com.investimentos.investimentos.enums.RiscoInvestimento;
import com.investimentos.investimentos.models.Investimento;
import com.investimentos.investimentos.models.Simulacao;
import com.investimentos.investimentos.models.Simular;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;
import java.util.Optional;

public class SimulacaoServicesTests {
    @Test
    public void testarSimularInvestimento() throws InvalidAttributeValueException {
        Investimento investimentoEntrada = new Investimento();
        investimentoEntrada.setId(1);
        investimentoEntrada.setNome("Mariana vai");
        investimentoEntrada.setDescricao("VFOooooi");
        investimentoEntrada.setRiscoInvestimento(RiscoInvestimento.ALTO);
        investimentoEntrada.setPercentual(0.50);
        Simular simular = new Simular(investimentoEntrada,10,100);

        SimulacaoService simulacaoService = new SimulacaoService();
        Simulacao simulacao = simulacaoService.simulacarInvestimento(simular);

        Assertions.assertEquals(simulacao.getResultadoSimulacao(), 105);
    }

    @Test
    public void testarSimularInvestimentoError() throws InvalidAttributeValueException {
        Investimento investimentoEntrada = new Investimento();
        investimentoEntrada.setId(1);
        investimentoEntrada.setNome("Mariana vai");
        investimentoEntrada.setDescricao("VFOooooi");
        investimentoEntrada.setRiscoInvestimento(RiscoInvestimento.ALTO);
        investimentoEntrada.setPercentual(0.50);
        Simular simular = new Simular(investimentoEntrada,10,10);

        SimulacaoService simulacaoService = new SimulacaoService();

        Assertions.assertThrows(InvalidAttributeValueException.class, () -> simulacaoService.simulacarInvestimento(simular));


    }

}
