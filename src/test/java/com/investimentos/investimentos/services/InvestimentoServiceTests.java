package com.investimentos.investimentos.services;

import com.investimentos.investimentos.enums.RiscoInvestimento;
import com.investimentos.investimentos.models.Investimento;
import com.investimentos.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.management.InvalidAttributeValueException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTests {

    @MockBean
    InvestimentoRepository investimentoRepository;

    @Autowired
    InvestimentoService investimentoService;

    Investimento investimento;

    @BeforeEach
    public void inicializar(){
        investimento = new Investimento("BCFF11", "Fundo de Investimento Imobiliário", RiscoInvestimento.BAIXO, 0.47);
    }

    @Test
    public void testarCriarInvestimentoSucesso() throws InvalidAttributeValueException {
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimento);

        Investimento investimentoObjeto = investimentoService.criarInvestimento(investimento);

        Assertions.assertEquals(investimento, investimentoObjeto);
        Assertions.assertEquals(investimento.getNome(), investimentoObjeto.getNome());
    }

    @Test
    public void testarBuscarTodosInvestimentos(){
        investimento.setId(1);
        Mockito.when(investimentoRepository.findAll()).thenReturn(Arrays.asList(investimento));

        Iterable<Investimento> investimentoIterable = investimentoService.buscarTodosInvestimentos();
        List<Investimento> retorno = (List)investimentoIterable;

        Assertions.assertEquals(1, retorno.size());
        Assertions.assertEquals(1, retorno.get(0).getId());
        Assertions.assertEquals("BCFF11", retorno.get(0).getNome());
    }

    @Test
    public void testarBuscarInvestimentoPorIdSucesso(){
        Optional<Investimento> retorno = Optional.of(investimento);

        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(retorno);

        Optional<Investimento> investimentoObjeto = investimentoService.buscarPorId(1);

        Assertions.assertEquals(retorno, investimentoObjeto);
    }

    @Test
    public void testarAtualizarInvestimento(){
        Investimento investimentoAtualizado = new Investimento();
        investimentoAtualizado.setId(1);
        investimentoAtualizado.setDescricao("FII - Fundo de Investimento Imobiliário.");
        Investimento investimentoComAlteracao = new Investimento();
        investimentoComAlteracao = investimento;
        investimentoComAlteracao.setDescricao("FII - Fundo de Investimento Imobiliário.");
        Optional<Investimento> retorno = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(retorno);
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(investimentoComAlteracao);

        Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimentoComAlteracao);

        Assertions.assertEquals(investimentoAtualizado.getDescricao(), investimentoObjeto.getDescricao());
    }

    @Test
    public void testarDeletarInvestimento(){
        investimentoService.deletarInvestimento(investimento);
        Mockito.verify(investimentoRepository, Mockito.times(1)).delete(Mockito.any(Investimento.class));
    }


}
