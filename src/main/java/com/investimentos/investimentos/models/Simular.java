package com.investimentos.investimentos.models;

import net.bytebuddy.implementation.bind.annotation.Empty;

import javax.persistence.Column;

public class Simular {
    protected Investimento investimento;
    protected int mesesDeAplicacao;
    protected double dinheiroAplicado;

    public Simular(Investimento investimento, int mesesDeAplicacao, double dinheiroAplicado) {
        this.investimento = investimento;
        this.mesesDeAplicacao = mesesDeAplicacao;
        this.dinheiroAplicado = dinheiroAplicado;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }

    public int getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(int mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}
