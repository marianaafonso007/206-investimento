package com.investimentos.investimentos.models;

import com.investimentos.investimentos.enums.RiscoInvestimento;

import javax.persistence.*;

@Entity
public class Investimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(nullable = false)
    private String nome;
    private String descricao;

    @Column(nullable = false)
    private RiscoInvestimento riscoInvestimento;
    @Basic
    @Column(nullable = false)
    private double percentual;

    public Investimento() {
    }

    public Investimento(String nome, String descricao, RiscoInvestimento riscoInvestimento, double percentual) {
        this.nome = nome;
        this.descricao = descricao;
        this.riscoInvestimento = riscoInvestimento;
        this.percentual = percentual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public RiscoInvestimento getRiscoInvestimento() {
        return riscoInvestimento;
    }

    public void setRiscoInvestimento(RiscoInvestimento riscoInvestimento) {
        this.riscoInvestimento = riscoInvestimento;
    }

    public double getPercentual() {
        return percentual;
    }

    public void setPercentual(double percentual) {
        this.percentual = percentual;
    }
}
