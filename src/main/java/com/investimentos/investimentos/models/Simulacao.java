package com.investimentos.investimentos.models;

public class Simulacao {

    private double resultadoSimulacao;

    public Simulacao() {
    }

    public double getResultadoSimulacao() {
        return resultadoSimulacao;
    }

    public void setResultadoSimulacao(double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }
}
