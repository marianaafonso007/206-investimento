package com.investimentos.investimentos.repositories;

import com.investimentos.investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
}
