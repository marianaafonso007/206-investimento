package com.investimentos.investimentos.controllers;

import com.investimentos.investimentos.models.Simulacao;
import com.investimentos.investimentos.models.Simular;
import com.investimentos.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.InvalidAttributeValueException;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @PutMapping()
    public ResponseEntity<Simulacao> simulacaoDeInvestimento(@RequestBody Simular simular) throws InvalidAttributeValueException {
        Simulacao resultado = simulacaoService.simulacarInvestimento(simular);
        return ResponseEntity.status(200).body(resultado);
    }

}
