package com.investimentos.investimentos.controllers;

import com.investimentos.investimentos.models.Investimento;
import com.investimentos.investimentos.services.InvestimentoService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.management.InvalidAttributeValueException;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @GetMapping("/{id}")
    public Investimento buscarInvestimento(@PathVariable Integer id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if(investimentoOptional.isPresent()){
            return investimentoOptional.get();
        }else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping()
    public ResponseEntity<Iterable<Investimento>> buscarInvestimento(){
        try {
            return ResponseEntity.status(200).body(investimentoService.buscarTodosInvestimentos());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<Investimento> criarInvestimento(@RequestBody @Validated Investimento investimento) throws InvalidAttributeValueException {
        Investimento investimentoObjeto = investimentoService.criarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObjeto);
    }

    @PutMapping("/{id}")
    public Investimento atualizarInvestimento(@PathVariable Integer id, @RequestBody Investimento investimento){
        investimento.setId(id);
        try {
            Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimento);
            return investimentoObjeto;
        }catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Investimento> deletarLead(@PathVariable Integer id){
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if(investimentoOptional.isPresent()){
            investimentoService.deletarInvestimento(investimentoOptional.get());
            return ResponseEntity.status(204).body(investimentoOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }

}
