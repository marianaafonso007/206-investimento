package com.investimentos.investimentos.services;

import com.investimentos.investimentos.models.Investimento;
import com.investimentos.investimentos.models.Simulacao;
import com.investimentos.investimentos.models.Simular;
import com.investimentos.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Simulacao simulacarInvestimento(Simular simular) throws InvalidAttributeValueException{
        if (simular.getDinheiroAplicado() < 99.99){
            throw new InvalidAttributeValueException("O investimento deve ser no valor mínimo de 100 reais.");
        }
        double resultadoSimulacao = 0;
        resultadoSimulacao = (simular.getDinheiroAplicado() * simular.getInvestimento().getPercentual())/100;
        resultadoSimulacao = resultadoSimulacao * simular.getMesesDeAplicacao();
        resultadoSimulacao = simular.getDinheiroAplicado() + resultadoSimulacao;
        Simulacao simulacao = new Simulacao();
        simulacao.setResultadoSimulacao(resultadoSimulacao);
        return simulacao;
    }

}
