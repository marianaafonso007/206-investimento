package com.investimentos.investimentos.services;

import com.investimentos.investimentos.models.Investimento;
import com.investimentos.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public  Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoRepository.findAll();
    }

    public Investimento criarInvestimento(Investimento investimento) {
        if(investimento.getPercentual() > 0){
            return investimentoRepository.save(investimento);
        }
        throw new ObjectNotFoundException("", "O valor do percentual deve ser maior que zero!");
    }

    public Optional<Investimento> buscarPorId(int id){
        Optional<Investimento> investimento = investimentoRepository.findById(id);
        if(investimento.isPresent())
        {
            return investimento;
        }
        throw new RuntimeException("Investimento not found!");
    }

    public void deletarInvestimento(Investimento investimento) {
        investimentoRepository.delete(investimento);
    }

    public Investimento atualizarInvestimento(Investimento investimento) {
        return  investimentoRepository.save(investimento);
    }


}
