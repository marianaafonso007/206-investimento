package com.investimentos.investimentos.enums;

public enum RiscoInvestimento {
    ALTO,
    BAIXO,
    MEDIO,
}
